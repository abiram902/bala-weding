import "./App.css";
import BgImage from "./assets/cakeBg.jpg";
import Location from "./assets/location.png";
import couples from "./assets/picture1.png";
/* import picture1 from "./assets/balaToon.png"; */
import picture1 from "./assets/brideToon1.png";
import picture2 from "./assets/balaToon1.png";

function App() {
  return (
    <div className="App">
      <div className="jumbotron">
        <div className="invite__container">
          <div className="main__image">
            <img src={couples} alt="Hello" width="200" />
          </div>

          <p className="balaWedsKani">
            Bala{" "}
            <span className="weds" style={{ fontSize: "3rem", color: "green" }}>
              weds
            </span>{" "}
            Kani
          </p>
          <p className="togetherWithFamily">
            Together with our families we invite
          </p>
          <p className="togetherWithFamily">
            You to The wedding Reception on{" "}
            <strong>
              11<sup>th</sup>
            </strong>
            Sept
          </p>
          <p className="togetherWithFamily"></p>
        </div>
      </div>

      <div className="wedding__date">
        <h1 className="save__the__date">Save The Date</h1>
        <div className="flex-container">
          <div className="date">
            <h3 className="date__heading">When</h3>
            <p className="date__details">Wedding Ceremony : 12th Sept, 9am</p>
            <p className="date__details">Wedding Reception : 11th Sept, 7pm</p>
            <h3 className="date__heading">Where</h3>
            <p className="date__details">Devangar kalyana mandapam,</p>
            <p className="date__details">Poo market, Coimbatore.</p>
            <a
              target="_blank"
              href="https://goo.gl/maps/nKbyYNCN4E3xmHar8"
              className="location__container"
              rel="noreferrer"
            >
              <img src={Location} alt="pin" width="30" />
              <p className="date__details location">Location</p>
            </a>
          </div>
          <div className="seperator"></div>
          <div className="images__container">
            <img
              src={picture1}
              alt="picture1"
              width="500"
              className="bala-image-animation picture1"
            />
            <img
              src={picture2}
              alt="picture2"
              width="500"
              className="bala-image-animation picture2"
            />
            <div className="light"></div>
            {/* <p className="date__details">Kabim kubam</p> */}
          </div>
        </div>
      </div>

      <img src={BgImage} alt="logo" className="background-image" />
    </div>
  );
}

export default App;
